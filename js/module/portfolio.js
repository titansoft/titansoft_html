define(['jquery', 'jquery.jcarousel', 'jquery-ui-1.10.0.custom.min'], function ($) {
	var carouselInit = null;

	$('#section-portfolio').scroll(function () {
		if (!carouselInit) {
			carouselInit = $('#portfolio-slider').jcarousel();

			var carousel = $('#portfolio-slider').data('jcarousel');

			$('#slider').slider({
				min: 1,
				max: $('#portfolio-slider li').length - 2,
				slide: function (event, ui) {
					carousel.scroll(ui.value, 0);
				}
			});

			/* Portfolio item */
			$('.open-portfolio').click(function () {
				initPortfolioItem($(this).data('id'));
			});

			$('.portfolio-close-btn').live('click', function () {
				$('.portfolio-item-block').fadeOut();
				$('.portfolio-items-container').fadeOut();
			})
		}
	});

	$('.portfolio-link-left').live('click', function () {
		var carousel = $('#portfolio-slider').data('jcarousel');
		carousel.prev();
		$('#slider').slider('value', carousel.first);
	});

	$('.portfolio-link-right').live('click', function () {
		var carousel = $('#portfolio-slider').data('jcarousel');
		carousel.next();
		$('#slider').slider('value', carousel.first);
	});

});

function initPortfolioItem(portfolioId) {

	var portfolioBlock = $('.portfolio-items-container .portfolio-item-block[data-id = ' + portfolioId + ']');

	portfolioBlock.fadeIn();
	$('.portfolio-items-container').fadeIn();

	if (portfolioBlock.find('.portfolio-item-gallery').data('jcarousel') === undefined) {
		for (var i = 0; i < $(portfolioBlock).find('.portfolio-item-gallery > li').length; i++) {
			$(portfolioBlock).find('.jcarousel-control').append($('<li>'));
		}

		$(portfolioBlock).find('.jcarousel-control li:first').addClass('active');

		$(portfolioBlock).find('.portfolio-item-gallery').jcarousel({
			scroll: 1,
			initCallback: mycarousel_initCallback,
			buttonNextHTML: null,
			buttonPrevHTML: null
		});
	}

}

function mycarousel_initCallback(carousel) {
	$('.jcarousel-control li').click(function () {
		$('.portfolio-item-gallery').data('jcarousel').scroll($(this).index() + 1);

		$('.jcarousel-control > li').removeClass('active');
		$(this).addClass('active');
	});
}

define(['jquery', 'jquery.scrollTo', 'jquery.easing'], function($) {
  var me={}, isExists=false;

  /* Init */
  $('.section').each(function(index) {
    var $this=$(this);
    $this.data('index',index);

    if($this.is('.active')) {
      $this.css({'left':'0'});
      isExists=true;
    }
    else if(isExists){
      $this.css({'left':'100%'});
    }
  });

  /* Protected methods */
  me._isActive=function($el) {
    return $el.is('.active')
  };

  me._moveToSection=function($section, isScrollTop) {
    var $active=$section.prevAll('.active'),
      animateSpeed=500,
      animateOptions={queue: false, easing: 'easeInOutCubic'};

    if(isScrollTop===true)
      $section.scrollTop();

    if ($active.length > 0)
      $active.animate({'left':'-100%'}, animateSpeed, null, animateOptions);
    else {
      $active = $section.nextAll('.active');
      $active.animate({'left':'100%'}, animateSpeed, null, animateOptions);
    }

    $active.removeClass('active');
    $section.addClass('active').animate({'left': '0%'},animateSpeed, null, animateOptions);
  };


  /* Public methods */
  /* Перемещение к активной странице */
  me.moveToActivePage=function() {
    var $page=$('.page.active:first');

    if($page.length>0)
      me.moveTo($page);
  };

  /* Перемещение к произвольной странице или разделу */
  me.moveTo=function($part) {
    var isSection=$part.is('.section'),
      $section, offset;

    if(isSection&&me._isActive($part)) return;

    if(isSection) {
      me._moveToSection($part, true);
      return;
    }

    $section=$part.parent();
    offset=$part.is('.first')?0:30;
    if(me._isActive($section))
      $section.scrollTo($part, 1000, {easing: 'easeOutExpo', offset: offset});
    else
      $section.scrollTo($part, 0, {easing: 'easeOutExpo', offset: offset, onAfter: function() {
        me._moveToSection($section, false);
      }});

    $('.page.active').removeClass('active');
    $part.addClass('active');

  };

  return me;
});
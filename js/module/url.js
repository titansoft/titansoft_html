define(['jquery', 'app/loader', 'app/navigation', 'jquery.history'], function($, loader, nav) {
  var me={}, map={}, history=window.History, uid=1,
    _pushToMap=function(id) {
      var $page;
      if(map[id]) return false;

      $page=$('#'+id);
      if(!$page.is('.page')) return false;

      map[id]=$page.find('.page-header').html();
      if(!map[id])
        map[id]=id;
      return true;
    };

  loader.subscribe('pageLoaded', function(data) {
    _pushToMap(data.id);
  });

  $('a.route').each(function() {
    var id=this.hash?'':this.hash.substring(1);
    if(!id) return;

    _pushToMap(id);

  }).click(function(e) {
    var id=this.hash?this.hash.substring(1):'';
    if(!map[id]) return;

    history.pushState({pageId: id, uid: uid++}, map[id], id);

    e.preventDefault();
  });

  history.Adapter.bind(window,'statechange',function(){
    var state = history.getState(),
      $page=$('#'+state.data.pageId);

    if(state.data.uid!=uid-1) {
      nav.moveTo($page);
    }
  });

  return me;
});

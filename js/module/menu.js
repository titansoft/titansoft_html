define(['jquery','jquery.scrollTo','jquery.easing', 'app/loader', 'app/navigation'], function($, f1, f2, loader, nav) {
  $('#menu').on('mouseover', function(e) {
    var $target=$(e.target),
      $menuWrapper=$('#section-menu'),
      section, $menu;

    if($.data(this, 'prevTarget')==e.target) return;
    $.data(this, 'prevTarget', e.target);

    if($target.is('.menu-link')) {
      section=$target.data('section') || false;

      if(section) {
        $menu=$('#'+section+'-menu');

        if($menuWrapper.is(':animated'))
          $menuWrapper.stop(true);

        if($menuWrapper.is('.opened')) {
          $menuWrapper.scrollTo($menu, 200, {'easing': 'linear'});
        }
        else {
          $menuWrapper.scrollTo($menu);
          $menuWrapper.addClass('opened');
        }

        $menuWrapper.animate({'height': $menu.outerHeight()}, {'duration': 200, 'queue': false});
      }
      else if($menuWrapper.is('.opened')) {
        $menuWrapper.animate({'height': 0}, 200, function() {
          $menuWrapper.removeClass('opened');
        });
      }
    }
  }).on('mouseleave', function(e) {
      var $target=$(e.target),
        $menuWrapper=$('#section-menu');

      if($target.is('#menu-container')) return;

      $menuWrapper.animate({'height': 0}, 200, function() {
        $menuWrapper.removeClass('opened');
      });
    });


  $('.route-page').click(function() {
    var $page=$(this.hash),
      $section=$page.parent(),
      $menuWrapper=$('#section-menu');

    loader.load($section, function() {
      nav.moveTo($page);
    });

    $menuWrapper.animate({'height': 0}, 200, function() {
      $menuWrapper.removeClass('opened');
    });
  });

  $('.route-section').click(function() {
    var $section=$(this.hash);
    loader.load($section, function() {
      nav.moveTo($section);
    });
  });
});
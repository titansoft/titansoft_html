define(['jquery', 'app/observer'], function($, observer) {
  var _isFunction=function(obj) { return Object.prototype.toString.call(obj) == '[object Function]';},
    loadedClass='loaded',
    $preloader=$('#loader'),
    me=$.extend({}, new observer());

  me._showPreLoader=function() {
    $('#loader').fadeIn(300);
  };
  me._hidePreLoader=function() {
    $('#loader').fadeOut(300);
  };

  /* Проверка загружена ли страница */
  me.isLoaded=function($el) {
    return $el.is('.'+loadedClass);
  };

  /* Загрузка активного раздела */
  me.loadActiveSection=function(callback) {
    var $section=$('.section.active:not(.'+loadedClass+')');
    if($section.length>0)
      me.load($section, callback);
  };

  /* Загрузка активной страницы */
  me.loadActivePage=function(callback) {
    var $page=$('.page.active:not(.'+loadedClass+')');
    if($page.length>0)
      me.load($page, callback);
  };

  /* Загрузка страницы или раздела */
  me.load=function($part, callback) {
    var arrLoaders=[], $parts,
      isSection=$part.is('.section');

    if(!_isFunction(callback))
      callback=function() {};

    if(me.isLoaded($part))
      return callback();

    me._showPreLoader();

    $parts=isSection?$part.find('.page:not(.'+loadedClass+')'):$part;

    $parts.each(function(index) {
      var $this=$(this),
        id=this.id,
        d=$.Deferred();

      $.ajax({url: 'html/'+id+'.html', dataType: 'html', success: function(html) {
        $this[0].innerHTML=html;
        $this.removeClass('hidden').addClass(loadedClass)
          .next('hr').removeClass('hidden');
        me.publish('pageLoaded', { id: id });
        d.resolve();
      }});


      arrLoaders.push(d);
    });

    $.when.apply($,arrLoaders).done(function() {
      if(isSection)
        $part.removeClass('hidden').addClass(loadedClass);
      else
        $part.parent().removeClass('hidden').addClass(loadedClass);

      me._hidePreLoader();
      setTimeout(function() { callback(); }, 300);
    });
  };

  return me;
});

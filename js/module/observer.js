define(function() {
  var _isFunction=function(obj) { return Object.prototype.toString.call(obj) == '[object Function]';},
    observer=function() {
      this._events={};
    };

  observer.prototype={
    _existsEvent: function(event) {
      return typeof this._events[event] !== 'undefined';
    },

    subscribe: function(event, callback) {
      if(!_isFunction(callback)) return false;

      if(!this._existsEvent(event))
        this._events[event]=[];

      this._events[event].push(callback);
      return true;
    },

    publish: function(event, data) {
      var i, l;
      if(this._existsEvent(event)) {
        for(i=0, l=this._events[event].length; i<l; i++) {
          if(this._events[event][i](data)===false)
            return;
        }
      }
    }
  };

  return observer;
});
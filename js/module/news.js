define(['jquery', 'jquery.jcarousel'], function ($) {

	function initNewsItemsCarousel() {
		$('#news_list').jcarousel({
			scroll: 1
		});

		$('.news-items-container .news-link-left').click(function () {
			$('#news_list').data('jcarousel').prev();
		});

		$('.news-items-container .news-link-right').click(function () {
			$('#news_list').data('jcarousel').next();
		});

		$('.news-items-container .news-close-btn').click(function () {
			$('.news-items-container').fadeOut();
		});
	}

	function openNewsItemsCarousel(id) {
		if ($('#news_list').data('jcarousel') === undefined) {
			initNewsItemsCarousel();
		}

		$('.news-items-container').fadeIn();
	}

	$('.news .news-header').live('click', function () {
		openNewsItemsCarousel(1);
	});

});
define(['jquery'], function ($) {
	($(function () {
		ymaps.ready(initMap);

		function initMap() {
			var titansoft = [56.823503, 60.625134],
				center = titansoft,
			/*myMap = new ymaps.Map('map', {
			 center: center,
			 zoom: 16,
			 behaviors: ['scrollZoom', 'DblClickZoom', 'drag']
			 }),
			 myPortfolioMap = new ymaps.Map('portfolio_map', {
			 center: center,
			 zoom: 16,
			 behaviors: ['scrollZoom', 'DblClickZoom', 'drag']
			 }),*/
				placemark = new ymaps.Placemark(titansoft, {
					name: 'Titansoft'
				}, {
					iconImageHref: 'img/titansoft-point.png',
					iconImageSize: [195, 229],
					iconImageOffset: [-100, -229],
					iconShadow: true,
					iconShadowImageHref: 'img/titansoft-point-shadow.png',
					iconShadowImageSize: [226, 111],
					iconShadowImageOffset: [-75, -110]
				}),
				myMap = null,
				myPortfolioMap = null;

			$('#section-main').scroll(function () {
				if ($('#map:visible').length != 0 && (myMap == null)) {
					myMap = new ymaps.Map('map', {
						center: center,
						zoom: 16,
						behaviors: ['scrollZoom', 'DblClickZoom', 'drag']
					});
					myMap.controls.add('mapTools');
					myMap.controls.add('zoomControl');
					myMap.controls.add('typeSelector');

					myMap.geoObjects.add(placemark);
				}
			});

			$('#section-portfolio').scroll(function () {
				if ($('#portfolio_map:visible').length != 0 && (myPortfolioMap == null)) {
					myPortfolioMap = new ymaps.Map('portfolio_map', {
						center: center,
						zoom: 16,
						behaviors: ['scrollZoom', 'DblClickZoom', 'drag']
					});

					myPortfolioMap.controls.add('mapTools');
					myPortfolioMap.controls.add('zoomControl');
					myPortfolioMap.controls.add('typeSelector');

					myPortfolioMap.geoObjects.add(placemark);
				}
			});


			return true;

		}
	}));
});

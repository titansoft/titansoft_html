define(['jquery', 'jquery.jcarousel', 'app/loader'], function ($) {

	$('#gallery_main').ready(function () {
		if ($('#gallery_main').data('jcarousel') === undefined) {
			$('#gallery_main > ul').fadeIn('fast', function () {

				$('#gallery_main').jcarousel({
					scroll: 1
				});

				$('.gallery-preview a').click(function () {
					$('#gallery_main').data('jcarousel').scroll($(this).index() + 1);
				});

			});
		}

	});
})
define(['jquery', 'app/loader'], function ($) {

	$('.vacancy-item-header').live('click', function () {
		if ($(this).hasClass('vacancy-item-opened')) {
			$(this).parent().find('.vacancy-content').slideUp();
			$(this).removeClass('vacancy-item-opened');
		} else {
			$(this).parent().find('.vacancy-content').slideDown();
			$(this).addClass('vacancy-item-opened');
		}
	});

});
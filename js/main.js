requirejs.config({
	baseUrl: 'js/lib',

	paths: {
		app: '../module'
	},

	shim: {
		'jquery.easing': ['jquery'],
		'jquery.scrollTo': ['jquery'],
		'jquery.history': ['jquery'],
		'jquery.placeholder': ['jquery'],
		'jquery.jcarousel': ['jquery'],
		'jquery-ui-1.10.0.custom.min': ['jquery']
	}
});

require(['jquery', 'app/navigation', 'app/loader', 'app/menu', 'app/url', 'app/map', 'app/portfolio', 'jquery.placeholder', 'jquery.jcarousel', 'app/news', 'app/vacancy', 'app/slider'], function ($, nav, loader) {
	/* Загружаем активный раздел */
	loader.loadActiveSection(function () {
		/* перемещаемся к нужной странице */
		nav.moveToActivePage();
	});

	/* Хелпер для подгонки высоты и ширины после ресайза */
	function SetWidthHeight() {
		$('.section').height($('#sections').height() - $('#header').outerHeight());
	}

	/* Динамически накидывает стили для разделов */
	$('#sections').addClass('section-wrap-absolute');

	/* Фиксируем шапку */
	$('#header').addClass('header-fixed');

	$(window).resize(SetWidthHeight);
	SetWidthHeight();

	var leftMenuItemWidth = $('.left-menu li:first').outerWidth(true);
	var leftMenuItemDifference = leftMenuItemWidth - 45;

	$('.left-menu li').each(function () {
		$(this).css('left', -leftMenuItemDifference);
	});

	$('.left-menu li').hover(function () {
		$(this).animate({left: 0}, 100);
	}, function () {
		$(this).animate({left: -leftMenuItemDifference + 'px'}, 100);
	});

	$('.left-menu-block').hide();
	var leftMenuVisible = false;

	$('#section-main').scroll(function () {
		if ($(this).scrollTop() > 480) {
			if (!leftMenuVisible) {
				leftMenuVisible = true;
				$('.left-menu-block').stop(true, true).fadeIn();
			}
		} else {
			if (leftMenuVisible) {
				leftMenuVisible = false;
				$('.left-menu-block').stop(true, true).fadeOut();
			}
		}
	});

	$('.left-menu a').click(function () {
		var target = $(this).attr('href');

//		$.scrollTo(target);
		$('#section-main').animate({scrollTop: $('#section-main').scrollTop() + $(target).offset().top - 120});
		return false;
	});


	$('.page-collapse-btn.close').live('click', function () {
		$($(this).data('target')).slideUp();
		$(this).hide();
		$(this).parent().find('.page-collapse-btn.open').css('display', 'inline-block');

	});

	$('.page-collapse-btn.open').live('click', function () {
		$($(this).data('target')).slideDown();
		$(this).hide();
		$(this).parent().find('.page-collapse-btn.close').css('display', 'inline-block');
	});

	$('[placeholder]').placeholder();

});